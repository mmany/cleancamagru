<?php
require "header.php";
?>
		<main>
			<div class="card">
				<h3 style="text-align:center">Login</h3>
				<hr />
<?php
if (isset($_GET['error']))
{
	echo '<div style="text-align:center">';
	if ($_GET['error'] == "emptyfields")
		echo            '<p class="error-msg">You need to fill all the fields</p>';
	if ($_GET['error'] == "wrongpwd")
		echo            '<p class="error-msg">Either the e-mail or the password is wrong</p>';
	if ($_GET['error'] == "noactivated")
		echo            '<p class="error-msg">You need to activate your e-mail before</p>';
	if ($_GET['error'] == "nouser")
		echo            '<p class="error-msg">This user does not exist</p>';
	echo '</div>';
}
else if (isset($_GET['login']))
{
	if ($_GET['login'] == "success")
		echo '<center><p class="success-msg">You are connected !</p></center>';
}
?>
				<form class="form-inline" action="manage_db/login.inc.php" method="post">
					<input class="form-control mr-2" type="text" name="uid" placeholder="Username">
					<input class="form-control mr-2" type="password" name="pwd" placeholder="Password">
					<div style="text-align:center">
						<button class="btn" type="submit" name="login-submit">Login</button>
					</div>
				</form>
				<a href="forgotPwd.php" style="text-align:right; font-size:12px">Forgot your password ?<a>
			</div>
		</main>
	</body>
</html>
<?php
require "footer.php";
?>
