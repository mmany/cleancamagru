<?php
require "header.php";
?>
		<main>
<?php if (isset($_SESSION['uidUsers']))
{ ?>
			<div class="card" style="width:1000px">
				<h3 style="text-align:center; font-size:30px">Settings</h3>
				<hr />
<?php
if (isset($_GET['error']))
{
	echo '<div style="text-align:center">';
	if ($_GET['error'] == "emptyfields")
		echo '<p class="error-msg">You need to fill in all the fields</p>';
	if ($_GET['error'] == "thatsnotyou")
		echo '<p class="error-msg">Hey ! That\'s not yours !</p>';
	//mail
	if ($_GET['error'] == "invalidmail")
		echo '<p class="error-msg">Invalid email</p>';
	if ($_GET['error'] == "emailtaken")
		echo '<p class="error-msg">An account with this mail already exists</p>';
	//login
	if ($_GET['error'] == "invalidusername")
		echo '<p class="error-msg">Invalid username</p>';
	if ($_GET['error'] == "usertaken")
		echo '<p class="error-msg">Username taken</p>';
	//password
	if ($_GET['error'] == "invalidpassword")
		echo '<p class="error-msg">Password must be at least 6 characters and contain one number, one lower case and one upper case character</p>';
	if ($_GET['error'] == "passwordcheck")
		echo '<p class="error-msg">Passwords don\'t match</p>';
	if ($_GET['error'] == "invalidoldpass")
		echo '<p class="error-msg">Wrong Old Password, please try again</p>';
}
else if (isset($_GET['settings']))
{
	if ($_GET['settings'] == "uidsuccess")
		echo '<center><p class="success-msg">You have changed your login successfully</p></center>';
	if ($_GET['settings'] == "mailsuccess")
		echo '<center><p class="success-msg">You have changed your mail successfully, don\'t forget to check your mail to validate your account</p></center>';
	if ($_GET['settings'] == "passwordsuccess")
		echo '<center><p class="success-msg">You have changed your password successfully</p></center>';
	if ($_GET['settings'] == "notifon")
		echo '<center><p class="success-msg">Notification activated !</p></center>';
	if ($_GET['settings'] == "notifoff")
		echo '<center><p class="success-msg">Notification deactivated !</p></center>';
}
?>
				<form class="form-inline" action="manage_db/modify.inc.php" method="post" style:"display: flex">
				<h3 style="text-align:center">Change Login</h3>
				<hr />
					<input class="form-control col-md-5" style="left:8%" type="text" name="olduid" placeholder="Old Username">
					<input class="form-control col-md-5" style="left:8%" type="text" name="newuid" placeholder="New Username">
					<div style="text-align:center">
						<button class="btn" type="submit" name="modifylogin-submit">Change Login</button></div><br>
				<center><img src="banni/icon.png" height=80px;></center>  
				<h3 style="text-align:center">Change Mail</h3>
				<hr />
				<form class="form-inline" action="manage_db/modify.inc.php" method="post">
					<input class="form-control col-md-5" style="left:8%" type="text" name="oldmail" placeholder="Old Mail">
					<input class="form-control col-md-5" style="left:8%" type="text" name="newmail" placeholder="New Mail">
					<div style="text-align:center">
						<button class="btn" type="submit" name="modifymail-submit">Change Mail</button></div><br>
				<center><img src="banni/icon.png" height=80px;></center>
				<h3 style="text-align:center">Change Password</h3>
				<hr />
				<form class="form-inline" action="manage_db/modify.inc.php" method="post">
					<input class="form-control col-md-3" style="left:4%" type="password" name="oldpass" placeholder="Old Password">
					<input class="form-control col-md-4" style="left:4%" type="password" name="newpass" placeholder="New Password">
					<input class="form-control col-md-4" style="left:4%" type="password" name="newpass2" placeholder="Repeat Password">
					<div style="text-align:center">
						<button class="btn" type="submit" name="modifypwd-submit">Change Password</button>
					</div>
				</form><br>
			<center><img src="banni/icon.png" height=80px;></center>
				<form action="manage_db/modify.inc.php" method="post">
				<h3 style="text-align:center">Do you want to receive an email alert when someone comments one of your picture ?</h3>
				<div style="text-align:center">
				<input type="radio" id="Notifyes" name="Notifyes" value="Yes" checked> Yes
				<input type="radio" id="Notifno" name="Notifyes" value="No"> No
				<button class="btn" type="submit" name="notif-submit">Update</button>
				</div>
				</form>
			</div>
<?php
}
else
{
?>
                                <div class="card">
                                <h3 style="text-align:center">Settings</h3>
                                <hr />
<?php
        echo    '<p style="text-align:center">You first need to Login or Register to access your settings</p>';
}
?>
                        </div>
		</main>
	</body>
</html>
<?php
require "footer.php";
?>
