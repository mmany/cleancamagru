<?php
require "header.php";
?>
		<main>
			<div class="card">
				<h3 style="text-align:center">No Password ? No problem !</h3>
				<hr />
<?php
if (isset($_GET['error']))
{
	echo '<div style="text-align:center"">';
	if ($_GET['error'] == "emptyfields")
		echo            '<p class="error-msg">You need to fill all the fields</p>';
	if ($_GET['error'] == "nomailfound")
		echo            '<p class="error-msg">There is no mail like yours</p>';
	if ($_GET['error'] == "invalidmail")
		echo            '<p class="error-msg">Come on.. That\'s not a real mail..</p>';
	echo '</div>';
}
else if (isset($_GET['success']))
{
	if ($_GET['success'] == "emailfound")
		echo '<center><p class="success-msg">A mail has been sent to this adress :)</p></center>';
}
?>
				<form class="form-inline" action="manage_db/forgotpwd.inc.php" method="post">
					<input class="form-control mr-2" type="text" name="mail" placeholder="Mail">
					<div style="text-align:center">
						<button class="btn" type="submit" name="forgotpwd-submit">Can I have a mail please ?</button>
					</div>
				</form>
			</div>
		</main>
	</body>
</html>
<?php
require "footer.php";
?>
