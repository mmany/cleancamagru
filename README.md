Camagru est un site de galerie photos avec possibilité de faire des photomontages.

J'ai choisi de faire le mien sur un theme Marvel, il s'appelle donc Camagroot !

Les étapes pour visualiser le site  :
- clone le repository
- modifier votre mot de passe de base de données dans config/database.php
(ligne "$DB_PASSWORD = 'mmany17';")
- executer config/setup.php (possibilité de le faire directement depuis le navigateur
via "http://localhost:8080/config/setup.php" si vous avez les réglages par défaut
de PhpMyAdmin)
- executer config/test.php (possibilité de le faire directement depuis le navigateur
via "http://localhost:8080/config/test.php" si vous avez les réglages par défaut
de PhpMyAdmin)

setup.php va créer la base de données
test.php est optionnel, il va générer 3 utilisateurs pour pouvoir directement 
tester le site.

Vous pouvez ensuite aller sur "http://localhost:8080/login.php" et tester le site.

