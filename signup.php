<?php
    require "header.php";
?>
        <main>
            <div class="card">
                <h3 style="text-align:center">Register</h3>
                <hr />
                <?php
                    if (isset($_GET['error']))
                    {
                        echo '<div style="text-align:center">';
                        if ($_GET['error'] == "emptyfields")
                            echo '<p class="error-msg">You need to fill in all the fields</p>';
                        if ($_GET['error'] == "invalidmailuid")
                            echo '<p class="error-msg">Invalid email or username</p>';
                        if ($_GET['error'] == "invalidmail")
							echo '<p class="error-msg">Invalid email</p>';
						if ($_GET['error'] == "emailtaken")
                            echo '<p class="error-msg">This email already exists</p>';
                        if ($_GET['error'] == "invaliduid")
							echo '<p class="error-msg">Invalid username</p>';
						if ($_GET['error'] == "invalidpassword")
                            echo '<p class="error-msg">Password must be at least 6 characters long and must contain one number, one lowercase and one uppercase character</p>';
                        if ($_GET['error'] == "passwordcheck")
                            echo '<p class="error-msg">Passwords don\'t match</p>';
                        if ($_GET['error'] == "usertaken")
							echo '<p class="error-msg">Username taken</p>';
					}
					else if (isset($_GET['signup']))
					{
                        if ($_GET['signup'] == "success")
                            echo '<center><p class="success-msg">You registered successfully <br/> Please validate your email</p></center>';
					}
                ?>
                <form action="manage_db/signup.inc.php" method="post">
                    <input type="text" name="uid" placeholder="Username">
                    <input type="text" name="mail" placeholder="E-mail">
                    <input type="password" name="pwd" placeholder="Password">
                    <input type="password" name="pwd-repeat" placeholder="Repeat password">
                    <div style="text-align:center">
                        <button class="btn" type="submit" name="signup-submit">Register</button>
                    </div>
                </form>
            </div>
        </main>
    </body>
</html>
<?php
require "footer.php";
?>
