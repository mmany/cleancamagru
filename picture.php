<!DOCTYPE html> 
<html lang="en">
	<meta name="viewport" content="width=device-width, user-scalable=yes">

<script type="text/javascript">

function init() {

	navigator.mediaDevices.getUserMedia({ audio: false, video: { width: 800, height: 600 } }).then(function(mediaStream) {

		var video = document.getElementById('sourcevid');
		video.srcObject = mediaStream;

		video.onloadedmetadata = function(e) {
			video.play();
		};

	}).catch(function(err) { console.log(err.name + ": " + err.message); });

}

function clone(){
	var vivi = document.getElementById('sourcevid');
	var canvas1 = document.getElementById('cvs').getContext('2d');
	canvas1.drawImage(vivi, 0,0, 300, 224);
	var base64=document.getElementById('cvs').toDataURL("image/png");	//l'image au format base 64
	document.getElementById('tar').value='';
	document.getElementById('tar').value=base64;
}

function uploadimg(){
	var imgdata=getElementById("#tar").setAttribute(data);
}
window.onload = init;
</script>
<body>
		<video id="sourcevid" height='80%' width='80%' autoplay="true"  style='display:inline'></video>
		<center><div style='top:700px, left:50px'>
<button onclick='clone()' id="takeapic" name="takeapic" class="buttonpic" disabled style='height:50px;width:80px,margin:auto'>Take a Pic !</button></div></center>

		<div id="main" style='height:150px;width:150px;margin:auto;display:inline'>
	<canvas id="cvs" height='225' width='300'></canvas>

		</div>
<form method="post">
<input id='tar' name="tar" hidden style='width:50%;height:200px;'></input>
<input type="text" id="filterchosen" name="filterchosen" hidden size="50"/>
<input type="submit" name="fork" class="buttonpic" value="Upload" onclick='uploadimg()'/>
</form>
<?php
if(isset($_POST['fork']))
{
	$filterchosen = $_POST['filterchosen'];
}
function transfertagain(){
	$ret        = false;
	$img_blob   = '';
	$img_taille = 0;
	$img_type   = '';
	$img_nom    = '';
	$taille_max = 250000000;
	$iduser     = $_SESSION['idUsers'];

	if ($img_taille > $taille_max) {
		echo "Trop gros !";
		return false;
	}

	$img_type = 'png';
	$img_nom  = "image$iduser";
	$img_blob = file_get_contents('tmp/image.png');
	$req = "INSERT INTO images (" .
		"nom_image, img_taille, img_type, id_uid, img_blob " .
		") VALUES (" .
		"'" . $img_nom . "', " .
		"'" . $img_taille . "', " .
		"'" . $img_type . "', " .
		"'" . $iduser . "', " .
		"'" . addslashes ($img_blob) . "') ";
	$bdd = connect();
	$req2 = $bdd->prepare($req);
	$result1 = $req2->execute();
	if ($result1)
		echo "Image Uploaded !\n<br>";
	else
		echo "Error, can't upload this image \n";
	$req2->closeCursor();
}
if(isset($_POST['tar']) && $data = $_POST['tar'])
{
	$data2=$data;
	list($type, $data) = explode(';', $data);
	list(, $data)      = explode(',', $data);
	$data = base64_decode($data);
	file_put_contents('tmp/image.png', $data);



	$_SESSION['filterchosen']=$filterchosen;
	function make_montage($filename, $newfile, $ext)
	{
		$mtg_img = $_SESSION['filterchosen'];
		if ($mtg_img == "montage1")
		{
			$selected_file = "imgs/spidey.png";
			$img_x = 150;
			$img_y = 150;
			$pos_x = 0;
			$pos_y = 80;
		}
		elseif ($mtg_img == "montage2")
		{
			$selected_file = "imgs/strange.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage3")
		{
			$selected_file = "imgs/thor2.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage4")
		{
			$selected_file = "imgs/groot.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage5")
		{
			$selected_file = "imgs/capameri.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage6")
		{
			$selected_file = "imgs/deadpool.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage7")
		{
			$selected_file = "imgs/capameri2.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage8")
		{
			$selected_file = "imgs/spidey2.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage9")
		{
			$selected_file = "imgs/capmarv1.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}
		elseif ($mtg_img == "montage10")
		{
			$selected_file = "imgs/wolverine.png";
			$img_x = 300;
			$img_y = 300;
			$pos_x = 0;
			$pos_y = 0;
		}

		$copy = imagecreatetruecolor(550, 450);

		imagealphablending($copy, false);
		imagesavealpha($copy, true);
		$source = imagecreatefrompng($selected_file);

		imagecopyresized($copy, $source, 0, 0, 0, 0, $img_x, $img_y, $img_x, $img_y);
		switch ($ext) {
		case "png":
			$destination = imagecreatefrompng('assets/images/'.$filename);
			break;
		case "PNG":
			$destination = imagecreatefrompng('assets/images/'.$filename);
			break;
		case "jpeg":
			$destination = imagecreatefromjpeg('assets/images/'.$filename);
			break;
		case "jpg":
			$destination = imagecreatefromjpeg('assets/images/'.$filename);
			break;      
		case "gif":
			$destination = imagecreatefromgif('assets/images/'.$filename);
			break;
		}

		function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
			$cut = imagecreatetruecolor($src_w, $src_h);
			imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
			imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
			imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
		}
		imagecopymerge_alpha($destination, $copy, $pos_x, $pos_y, 0, 0, $img_x, $img_y, 100);
		$success = imagepng($destination, $newfile);
		imagepng($destination, "tmp/image.png");
	}
	if(isset($_POST['fork']))
	{

		$data_img = $data2;
		if (preg_match('/^data:image\/(\w+);base64,/', $data_img, $type)) {
			$data_img = substr($data_img, strpos($data_img, ',') + 1);
			$type = strtolower($type[1]); // jpg, png

			if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
				echo "Seule une image au format jpg, jpeg ou png est attendue.";
				// header('location: montage.php');
				return;
			}

			$data_img = base64_decode($data_img);

			if ($data_img === false) {
				echo "Une erreur est survenue, veuillez réessayer.";
				//  header('location: montage.php');
				return;
			}
		} else {
			echo "$data_img Veuillez prendre une photo.";
			//  header('location: montage.php');
			return;
		}
		$filename = md5(uniqid(rand('9999999','999999999999999'), true)).".{$type}";
		$newfile = dirname(__FILE__).'/assets/images/'.$filename;
		file_put_contents($newfile, $data_img);
		$test2 = $_SESSION['filterchosen'];
		if ($test2 != NULL)
		{
			make_montage($filename, $newfile, "png");
			transfertagain();
		}
	}
}
?>
</body>
</html>
