<?php
require "../config/database.php";
$username = $_GET['uid'];
$keyff = $_GET['keyf'];
$sql = "SELECT keyf,activated FROM users WHERE uidUsers like :uid";
$req = $bdd->prepare($sql);
if($req->execute(['uid' => $username]) && $row =$req->fetch())
{
    $req->closeCursor();
    $keybdd = $row['keyf'];
    $activated = $row['activated'];
}
if($activated == '1') // Si le compte est déjà actif on prévient
  header("Location: ../index.php?activated=already");
else // Si ce n'est pas le cas on passe aux comparaisons
  {
      if($keyff == $keybdd) // On compare nos deux clés
       {
        // La requête qui va passer notre champ actif de 0 à 1
        $sql = "UPDATE users SET actif = 1 WHERE nameUsers like :username";
        $req = $bdd->prepare($sql);
        $req->execute(array('uid' => $username));
        // Si elles correspondent on active le compte !
        header("Location: ../index.php?activated=success");
       }
       else // Si les deux clés sont différentes on provoque une erreur...
        header("Location: ../index.php?activated=error");
  }
?>
