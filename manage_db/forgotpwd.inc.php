<?php
require "../header.php";
require "mailActivation.php";

if (isset($_POST['forgotpwd-submit']))
{
    $mail = $_POST['mail'];

    function connect(){
    require_once "../config/database.php";
    try{
        $bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch(PDOException $e){
            echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
        }
    return($bdd);
    }

    if (empty($mail))
    {
        header("Location: ../forgotPwd.php?error=emptyfields");
        exit();
	}
	else if (!filter_var($mail, FILTER_VALIDATE_EMAIL))
    {
        header("Location: ../forgotPwd.php?error=invalidmail");
        exit();
    }
    $bdd = connect();
    $sql = "SELECT emailUsers FROM users WHERE emailUsers=:mail";
    $req = $bdd->prepare($sql);
    $req->execute(['mail' => $mail]);
    if($mail = $req->fetch()['emailUsers']) //check if a line contain the same email
	{
		$req->closeCursor();
		$req = $bdd->prepare("SELECT keyf FROM users WHERE emailUsers= :mail");
		$req->execute(['mail' => $mail]);
		$keyfs = $req->fetch()['keyf'];
		$req->closeCursor();
        $req = $bdd->prepare("SELECT uidUsers FROM users WHERE emailUsers= :mail");
        $req->execute(['mail' => $mail]);
        $username = $req->fetch()['uidUsers'];
		forgotMailPwd($mail, $keyfs, $username);
		header("Location: ../forgotPwd.php?success=emailfound");
        $req->closeCursor();
        exit();
    }
    else
    {
        header("Location: ../forgotPwd.php?error=nomailfound");
        exit();
    }
}
require "../footer.php";
?>
