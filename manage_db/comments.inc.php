<?php
require "../header.php";
if (isset($_POST['comment-submit']))
{
	$comment = htmlspecialchars($_POST['comment']);
	$idimg = $_POST['idimg'];
	$mailUsr = $_POST['mailUsr'];
	$notif = $_POST['notif'];
	$page = $_POST['page'];

	function connect(){
		require_once "../config/database.php";
		try{
			$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		catch(PDOException $e){
			echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
		}
		return($bdd);
	}

	if (!isset($_SESSION['uidUsers']))
	{
		header("Location: ../index.php?error=noaccount");
		exit();
	}
	if (empty($comment) || empty($idimg))
	{
		header("Location: ../index.php?error=emptyfields");
		exit();
	}
	else
	{
		$bdd = connect();
		$idusr = $_SESSION['idUsers'];
		$sql = "INSERT INTO comments (" .
			"stockcomment, id_img, id_usr_com" .
			") VALUES (" .
			"'" . addslashes($comment) . "', " .
			"'" . $idimg . "', " .
			"'" . $idusr . "') ";
		$req = $bdd->prepare($sql);
		if ($req->execute())
		{
			if ($notif == 1)
			{
				require_once "mailActivation.php";
				commentMail($mailUsr);
			}
		if ($page == "index");
                	header("Location: ../index.php#$idimg");
                if ($page == "profile")
                        header("Location: ../profile.php#$idimg");
            exit();
		}
		else
		{
			header("Location: ../index.php?error=problembdd");
			exit();
		}
	}
}
?>
