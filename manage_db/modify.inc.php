<?php
require "../header.php";
require "mailActivation.php";

function connect(){
	require_once "../config/database.php";
	try{
		$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}
	catch(PDOException $e){
		echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
	}
	return($bdd);
}

//MODIFY LOGIN
if (isset($_POST['modifylogin-submit']))
{
	$olduid = $_POST['olduid'];
	$newuid = $_POST['newuid'];

	if (empty($olduid) || empty($newuid))
	{
		header("Location: ../settings.php?error=emptyfields");
		exit();
	}
	else if (!preg_match("/^[a-zA-Z0-9]*$/", $newuid))
	{
		header("Location: ../settings.php?error=invalidusername");
		exit();
	}
	else
	{
		$bdd = connect();
		$username= $_SESSION['uidUsers'];
		$sql = "SELECT uidUsers FROM users WHERE uidUsers= :newuid";
		$req = $bdd->prepare($sql);
		$req->execute(['newuid' => $newuid]);
		if ($olduid !== $username)
		{
			header("Location: ../settings.php?error=thatsnotyou");
			$req->closeCursor();
			exit();
		}
		else if($checkname = $req->fetch()) //check if a line contain the same name
		{
			header("Location: ../settings.php?error=usertaken");
			$req->closeCursor();
			exit();
		}
		/*else if ($olduid !== $username)
		{
			header("Location: ../settings.php?error=thatsnotyou");
			$req->closeCursor();
			exit();
		}*/
		else
		{
			$sql = "UPDATE `users` SET `uidUsers`= :newuid WHERE uidUsers='$username'";
			$req = $bdd->prepare($sql);
			$req->execute(['newuid' => $newuid]);
			$req->closeCursor();
			$_SESSION['uidUsers']= $newuid;
			header("Location: ../settings.php?settings=uidsuccess");
			exit();
		}
	}
}

// MODIFY EMAIL
if (isset($_POST['modifymail-submit']))
{
	$oldmail = $_POST['oldmail'];
	$newmail = $_POST['newmail'];

	if (empty($oldmail) || empty($newmail))
	{
		header("Location: ../settings.php?error=emptyfields");
		exit();
	}
/*	else if (!filter_var($mail, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username))
	{
		header("Location: ../settings.php?error=invalidmailuid");
		exit();
}*/
	else if (!filter_var($newmail, FILTER_VALIDATE_EMAIL))
	{
		header("Location: ../settings.php?error=invalidmail");
		exit();
	}
	else if ($_SESSION['emailUsers'] != $oldmail)
	{
		header("Location: ../settings.php?error=thatsnotyou");
		exit();
	}
	$bdd = connect();
	$sql = "SELECT emailUsers FROM users WHERE emailUsers= :newmail";
	$req = $bdd->prepare($sql);
	$req->execute(['newmail' => $newmail]);
	if($checkname = $req->fetch()) //check if a line contain the same email
	{
		header("Location: ../settings.php?error=emailtaken");
		$req->closeCursor();
		exit();
	}
	else
	{
		$username= $_SESSION['uidUsers'];
		$newkeyf = md5(microtime(TRUE)*100000);
		changingMail($username, $newmail, $newkeyf, $oldmail);
	//	changeOld($username, $oldmail);
		$sql = "UPDATE `users` SET `emailUsers`= :newmail, `keyf`= :newkeyf , `activated`= 0 WHERE uidUsers='$username'";
		$req = $bdd->prepare($sql);
		$req->execute(['newmail' => $newmail, 'newkeyf' => $newkeyf]);
		$req->closeCursor();
		$_SESSION['emailUsers']=$newmail;
		header("Location: ../settings.php?settings=mailsuccess");
		exit();
	}
}

//MODIFY PASSWORD
if (isset($_POST['modifypwd-submit']))
{
	$oldpass = $_POST['oldpass'];
	$newpass = $_POST['newpass'];
	$newpass2 = $_POST['newpass2'];

	$username= $_SESSION['uidUsers'];
	$bdd = connect();
	$sql = "SELECT * FROM users WHERE uidUsers= :username";
	$req = $bdd->prepare($sql);
	$req->execute(['username' => $username]);
	if (empty($oldpass) || empty($newpass) || empty($newpass2))
	{
		header("Location: ../settings.php?error=emptyfields");
		exit();
	}
	if ($row = $req->fetch())
	{
		$pwdCheck = password_verify($oldpass, $row['pwdUsers']);
		$req->closeCursor();
		if ($pwdCheck == false)
		{
			header("Location: ../settings.php?error=invalidoldpass");
			exit();
		}
		else if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$#', $newpass))
		{
			header("Location: ../settings.php?error=invalidpassword");
			exit();
		}
		else if ($newpass !== $newpass2)
		{
			header("Location: ../settings.php?error=passwordcheck");
			exit();
		}
		else
		{
			$hashedNewPwd = password_hash($newpass, PASSWORD_DEFAULT);
			$sql2 = "UPDATE users SET pwdUsers=:hashedNewPwd WHERE uidUsers='$username'";
			$req2 = $bdd->prepare($sql2);
			$req2->execute(['hashedNewPwd' => $hashedNewPwd]);
			$req2->closeCursor();
			$_SESSION['pwdUsers']= $hashedNewPwd;
			header("Location: ../settings.php?settings=passwordsuccess");
			exit();
		}
	}
}



// MODIFY NOTIF

if (isset($_POST['notif-submit']))
{
	$answer = $_POST['Notifyes'];

	$username= $_SESSION['uidUsers'];
	$bdd = connect();
	if ($answer == "No")
	{
		$sql8 = "UPDATE users SET receiveMail=2 WHERE uidUsers='$username'";
		$req8 = $bdd->prepare($sql8);
		$req8->execute();
		$req8->closeCursor();
		header("Location: ../settings.php?settings=notifoff");
		exit();
	}
	else
	{
		$sql8 = "UPDATE users SET receiveMail=1 WHERE uidUsers='$username'";
		$req8 = $bdd->prepare($sql8);
		$req8->execute();
		$req8->closeCursor();
		header("Location: ../settings.php?settings=notifon");
	}
}
?>
