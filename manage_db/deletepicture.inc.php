<?php
require "../header.php";
if (isset($_POST['delete-submit']))
{
	$idimg = $_POST['idimg'];
	$uidusr = $_POST['loginusr'];

	function connect(){
		require_once "../config/database.php";
		try{
			$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		catch(PDOException $e){
			echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
		}
		return($bdd);
	}

	if (empty($uidusr))
	{
		header("Location: ../profile.php?error=needlogin");
		exit();
	}
	else
	{
		$bdd=connect();
		$sql = "DELETE FROM images WHERE idimage ='$idimg' AND id_uid ='$uidusr'";
		$req = $bdd->prepare($sql);
		$req->execute();
		$req->closeCursor();
		header("Location: ../profile.php");
		exit();
	}
}
