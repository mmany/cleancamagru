<?php
require "../header.php";
if (isset($_POST['login-submit']))
{
    $uid = $_POST['uid'];
	$password = $_POST['pwd'];

    function connect(){
    require_once "../config/database.php";
    try{
        $bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch(PDOException $e){
            echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
        }
    return($bdd);
    }

    if (empty($uid) || empty($password))
    {
        header("Location: ../login.php?error=emptyfields");
        exit();
    }
    else
	{
		$bdd = connect();
		$sql = "SELECT * FROM users WHERE uidUsers= :uid";
        $req = $bdd->prepare($sql);
        $req->execute(['uid' => $uid]);
        if ($row = $req->fetch())
		{
            $pwdCheck = password_verify($password, $row['pwdUsers']);
            $activated = $row['activated'];
            if ($pwdCheck == false)
            {
                header("Location: ../login.php?error=wrongpwd");
                exit();
            }
            else if ($activated == 0)
            {
                header("Location: ../login.php?error=noactivated");
                exit();
            }
            else if ($pwdCheck == true)
            {
				$_SESSION['uidUsers'] = $row['uidUsers'];
				$_SESSION['emailUsers'] = $row['emailUsers'];
				$_SESSION['pwdUsers'] = $row['pwdUsers'];
				$_SESSION['idUsers'] = $row['idUsers'];
                header("Location: ../gallery.php?login=success");
                exit();
            }
        }
        else
        {
            header("Location: ../login.php?error=nouser");
            exit();
		}
	}
}
?>
