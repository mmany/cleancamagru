<?php
require "../header.php";

function connect(){
	require "../config/database.php";
try
{
	$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch(PDOException $e)
{
	echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
}
return($bdd);
}

$bdd = connect();

if (isset($_POST['like-submit']))
{
	$idimg = $_POST['isliked'];
	$idliker = $_POST['idus'];
	$page = $_POST['page'];

	if (empty($idliker))
	{
		header("Location: ../index.php?error=needlogin");
		exit();
	}
	/*CHECK IF ALREADY LIKED*/
	$sql = "SELECT COUNT(*) FROM managelike WHERE idimg = '$idimg' AND id_usr_like = '$idliker'";
	$req = $bdd->prepare($sql);
	$req->execute();
	$req->bindColumn(1, $exist);
	//	$req->bindColumn(2, $nameusr);
	$req->fetch();
	//	if(($idimg == $nbimg) && ($idliker == $nameusr))
	if ($exist)
	{
		$sql = "DELETE FROM managelike WHERE idimg ='$idimg' AND id_usr_like ='$idliker'";
		$req = $bdd->prepare($sql);
		$req->execute();
		$req->closeCursor();
		if ($page == "index");
		header("Location: ../index.php#$idimg");
		if ($page == "profile")
			header("Location: ../profile.php#$idimg");
		exit();
	}
	else{
		$sql = "INSERT INTO managelike (idimg, id_usr_like) VALUES ($idimg, $idliker)";
		$req = $bdd->prepare($sql);
		$req->execute();
		$req->closeCursor();
		if ($page == "index");
		header("Location: ../index.php#$idimg");
		if ($page == "profile")
			header("Location: ../profile.php#$idimg");
		exit();
	}
}
?>
