<?php
require "../header.php";
require "mailActivation.php";

if (isset($_POST['signup-submit']))
{
	$mail = $_POST['mail'];
	$username = $_POST['uid'];
	$password = $_POST['pwd'];
	$password2 = $_POST['pwd-repeat'];

	function connect(){
		require_once "../config/database.php";
		try{
			$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		catch(PDOException $e){
			echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
		}
		return($bdd);
	}

	if (empty($username) || empty($mail) || empty($password) || empty($password2))
	{
		header("Location: ../signup.php?error=emptyfields");
		exit();
	}
	else if (!filter_var($mail, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username))
	{
		header("Location: ../signup.php?error=invalidmailuid");
		exit();
	}
	else if (!filter_var($mail, FILTER_VALIDATE_EMAIL))
	{
		header("Location: ../signup.php?error=invalidmail");
		exit();
	}
	else if (!preg_match("/^[a-zA-Z0-9]*$/", $username))
	{
		header("Location: ../signup.php?error=invalidusername");
		exit();
	}
	else if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$#', $password))
    {
	header("Location: ../signup.php?error=invalidpassword");
	exit();
}
	else if ($password !== $password2)
	{
		header("Location: ../signup.php?error=passwordcheck");
		exit();
	}
	else
	{
		/*CHECK IF NAME TAKEN*/
		$bdd = connect();
		$sql = "SELECT uidUsers FROM users WHERE uidUsers= :username";
		$req = $bdd->prepare($sql);
		$req->execute(['username' => $username]);
		if($checkname = $req->fetch()) //check if a line contain the same name
		{
			header("Location: ../signup.php?error=usertaken");
			$req->closeCursor();
			exit();
		}
		/*CHECK IF EMAIL TAKEN*/
		$sql = "SELECT emailUsers FROM users WHERE emailUsers= :mail";
		$req = $bdd->prepare($sql);
		$req->execute(['mail' => $mail]);
		if($checkname = $req->fetch()) //check if a line contain the same email
		{
			header("Location: ../signup.php?error=emailtaken");
			$req->closeCursor();
			exit();
		}
		else
		{   
			$keyf = md5(microtime(TRUE)*100000);
			activateMail($username, $mail, $keyf);
			$sql = "INSERT INTO users (uidUsers, emailUsers, pwdUsers, keyf) VALUES (:username, :mail, :pwd, :keyf)";
			$hashedPwd = password_hash($password, PASSWORD_DEFAULT);
			$req = $bdd->prepare($sql);
			$req->execute(['username' => $username, 'mail' => $mail, 'pwd' => $hashedPwd, 'keyf' => $keyf]);
			$req->closeCursor();
			header("Location: ../signup.php?signup=success");
			exit();
		}
	}
}
?>
