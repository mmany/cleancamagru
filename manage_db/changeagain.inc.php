<?php
require "../header.php";
require "mailActivation.php";

$username = $_SESSION["uidUsers"];
$keyff = $_SESSION["keyf"];

function connect(){
	require "../config/database.php";
    try{
        $bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }
    catch(PDOException $e){
        echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
    }
    return($bdd);
}

//MODIFY PASSWORD
if (isset($_POST['modifypwdagain-submit']))
{
    $newpass = $_POST['newpass'];
    $newpass2 = $_POST['newpass2'];

    $bdd = connect();
    $sql = "SELECT * FROM users WHERE uidUsers= :username";
    $req = $bdd->prepare($sql);
    $req->execute(['username' => $username]);
    if (empty($newpass) || empty($newpass2))
    {
        header("Location: ../changePwdLink.php?error=emptyfields");
        exit();
	}
	if($row =$req->fetch())
{
    $req->closeCursor();
    $keybdd = $row['keyf'];
	}
      if($keyff !== $keybdd) // On compare nos deux clés
       {
		   header("Location: ../changePwdLink.php?error=keyinvalid");
	  }
    else if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$#', $newpass))
    {
            header("Location: ../changePwdLink.php?error=invalidpassword");
            exit();
     	}
        else if ($newpass !== $newpass2)
        {
            header("Location: ../changePwdLink.php?error=passwordcheck");
            exit();
        }
        else
        {
            $hashedNewPwd = password_hash($newpass, PASSWORD_DEFAULT);
            $sql2 = "UPDATE users SET pwdUsers=:hashedNewPwd WHERE uidUsers='$username'";
            $req2 = $bdd->prepare($sql2);
            $req2->execute(['hashedNewPwd' => $hashedNewPwd]);
			$req2->closeCursor();
			session_unset();
        	session_destroy();
            header("Location: ../changePwdLink.php?change=passwordsuccess");
            exit();
        }
    }

?>
