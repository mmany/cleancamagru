<?php
require "header.php";

if (isset($_GET['error']))
{
	echo '<div>';
	if ($_GET['error'] == "emptyfields")
		echo '<p class="error-msg">You need to write something</p>';
	if ($_GET['error'] == "noaccount")
		echo '<p class="error-msg">You first need to create an account or login</p>';
	if ($_GET['error'] == "problembdd")
		echo '<p class="error-msg">Sorry, something went wrong, try again later.</p>';
}
else if (isset($_GET['actif']))
{
	echo "<div class=\"card\">";
	if ($_GET['actif'] == "success")
		echo '<center><p class="success-msg">Votre email a bien été validé <br /> Vous pouvez maintenant vous connecter</p></center>';
	if ($_GET['actif'] == "already")
		echo '<center><p class="success-msg">Cet email a déjà été activé</p></center>';
	if ($_GET['actif'] == "error")
		echo '<center><p class="error-msg">Une erreur est intervenue, merci de réessayer</p></center>';
	echo "</div>";
}

function connect(){
	require "config/database.php";
try
{
	$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch(PDOException $e)
{
	echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
}
return($bdd);
}

$bdd = connect();



/*CREATE PAGINATION*/


$sql = "SELECT COUNT(*) AS id FROM images";
$retour = $bdd->prepare($sql);
$retour->execute();
$articlesparpage=9;
$articles=$retour->fetch(PDO::FETCH_ASSOC);
$totaldesarticles=$articles['id'];

$nombredepage=ceil($totaldesarticles/$articlesparpage);

echo '<center><li style="margin-left:auto; margin-right:auto;display:inline-block;">';
for ($i=1;$i<= $nombredepage;$i++)
{
	echo '<a class="indexgallery" href="http://127.0.0.1:8080/index.php?page=' . $i . '">' . $i . '</a>';
}
echo "</li></center><br />";
if (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page']>0 && $_GET['page']<= $nombredepage)
{
	$page=intval($_GET['page']);
}
else
{
	$page=1;
}

$premierarticleafficher=$page*$articlesparpage-$articlesparpage;

$sql = "SELECT idimage,img_blob FROM images ORDER BY idimage DESC LIMIT $premierarticleafficher , $articlesparpage";
$reponse = $bdd->prepare($sql);
$reponse->execute();


/*END PAGINATION*/
?>






	<div class="container">
	  <div class="gallerieindex">

<?php
$res = $reponse->fetchAll();
foreach ($res as $value)
{
	$sql8 = "SELECT id_uid FROM images WHERE idimage=".$value['idimage']."";
    $q8 = $bdd->prepare($sql8);
    $q8->execute();
    $q8->bindColumn(1, $uidtest);
    $q8->fetch();
    $q8->closeCursor();

    $sql9 = "SELECT uidUsers FROM users WHERE idUsers ='$uidtest'";
    $q9 = $bdd->prepare($sql9);
    $q9->execute();
    $q9->bindColumn(1, $uidposter);
    $q9->fetch();
	$q9->closeCursor();


	file_put_contents("imgsmontage/".$value['idimage'].".png", $value['img_blob']);
?>
	<div class="col-6 col-sm-4">
	<div class="detailBox" id="<?php echo $value['idimage']?>">
	<div class="commentBox">
<?php   
echo "<img src='"."imgsmontage/".$value['idimage'].".png' width='300px'>";
?>   <center><?php echo "Posted by $uidposter";?> </center> <?php
$_SESSION['idimage']= $value['idimage'];
$idimage = $_SESSION['idimage'];?>
	</div>
	<div class="actionBox">




<?php


// LIKE SYSTEM


$sql4 = "SELECT COUNT(*) FROM managelike WHERE idimg ='$idimage'";
$q4 = $bdd->prepare($sql4);
$q4->execute();
$q4->bindColumn(1, $nblikes);
$q4->fetch();
$q4->closeCursor();
if(isset($_SESSION['idUsers']))
{
	$iduse = $_SESSION['idUsers'];
	$sql5 = "SELECT COUNT(*) FROM managelike WHERE idimg = '$idimage' AND id_usr_like = '$iduse'";
	$q5 = $bdd->prepare($sql5);
	$q5->execute();
	$q5->bindColumn(1, $active);
	$q5->fetch();
	$q5->closeCursor();
	if($active == 1)
		$active= "red";
}
if (!isset($active))
	$active= "grey; cursor: not-allowed;";?>
<?php if(isset($_SESSION['idUsers']))
	{?>
<form action="manage_db/managelikes.php" method="post">
<input value="<?php echo "".$idimage.""?>" hidden name="isliked"/>
<input value="index" hidden name="page"/>
<input value="<?php echo "".$iduse.""?>" hidden name="idus"/>
<button class="heartlike" style="color:<?php echo "".$active."" ?>" type=submit name="like-submit" for="heart<?php echo "".$idimage.""?>">♡ </button>
<label><?php echo "".$nblikes."" ?></label>
</form>
<?php }
else
{ ?>
<label class="heartlike">♡ </label>
<label><?php echo "".$nblikes."" ?></label>
<?php } ?>
		<ul class="commentList">
			<li>
				<div class="commenterImage">
				</div>
				<div class="commentText">
<?php


// COMMENT SYSTEM


$sql = "SELECT stockcomment,id_usr_com FROM comments WHERE id_img ='$idimage' ORDER BY idcomment DESC";
$q = $bdd->prepare($sql);
$q->execute();
$q->bindColumn(1, $commenttowrite);
$q->bindColumn(2, $iduser);
while($q->fetch())
{
	$sql4 = "SELECT uidUsers FROM users WHERE idUsers='$iduser'";
	$q4 = $bdd->prepare($sql4);
	$q4->execute();
	$q4->bindColumn(1, $uiduser);
	$q4->fetch();
?>
	<p><?php echo $commenttowrite ?></p> <span class="sub-text">by <?php echo $uiduser ?></span><hr color="grey"/>
<?php
	$sql7 = "SELECT id_uid,idimage FROM images WHERE idimage='$idimage'";
	$q7 = $bdd->prepare($sql7);
	$q7->execute();
	$q7->bindColumn(1, $idcompare);
	$q7->fetch();
	$q7->closeCursor();

	$sql6 = "SELECT emailUsers,receiveMail FROM users WHERE idUsers ='$idcompare'";
	$q6 = $bdd->prepare($sql6);
	$q6->execute();
	$q6->bindColumn(1, $mailUsr);
	$q6->bindColumn(2, $notif);
	$q6->fetch();
	$q6->closeCursor();
}?>
				</div>
			</li>
		</ul>
		<form class="form-inline" role="form" method="POST" action="manage_db/comments.inc.php">
			<div class="form-group">
				<input class="form-control" type="text" name="comment" placeholder="Your comments" />
				<input id="idimg<?php echo "".$idimage.""?>" name="idimg" type="hidden" value="<?php echo "".$idimage.""?>">

				<input id="mailUsr" name="mailUsr" type="hidden" value="<?php if(isset($mailUsr)) { echo "".$mailUsr.""; }?>">
<input value="index" hidden name="page"/>
								<input id="notif" name="notif" type="hidden" value="<?php if(isset($notif)) { echo "".$notif.""; }?>">
			</div>
			<div class="form-group">
				<button class="btn btn-default" name="comment-submit">Add</button>
			</div>
		</form>
	</div>
</div>
	</div>
<?php
}
?>

</div>
</div>

<?php 
$reponse->closeCursor();
//$q->closeCursor();
//$q2->closeCursor();
//$q4->closeCursor();
?>

<?php
require "footer.php";
?>
