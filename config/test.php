<?php
function connect(){
	require "database.php";
	try{
		$bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}
	catch(PDOException $e){
		echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
	}
	return($bdd);
}

$bdd = connect();
$sql1 = "INSERT INTO users (`uidUsers`, `emailUsers`, `pwdUsers`, `activated`, `receiveMail`, `keyf`) 
	VALUES 
('test', 'mariemany17@yahoo.fr', '$2y$10\$AHqq639r1ww28BXZmLs9C.WBmDsdLS7/tiOeW3znhoka35bqKiOzi', 1, 1, 'ca6a8a260f188566a84ecd287d9f55b9'),
('test2', 'marievanhaekendover@gmail.com', '$2y$10\$saHYCcpJ78yMxlF16CG87OEMXjevDE..HHo.sxah0qyMEkUoWk4Ea', 1, 1, '36fd857a4bd334993f5fb3ee6f6fe67d'),
('mmany', 'mmany@student.42.fr', '$2y$10$3paYimpz/qoPp3UV5IJe/O8vvuc564zMxViPA.bBGJOX0CurSuaO6', 1, 1, '6e30e8b0e02224a97700124457fd3b0a');
 ";
$req2 = $bdd->prepare($sql1);
$result1 = $req2->execute();
if ($result1)
	echo "Created users tests\n<br>";
else
	echo "Erreur lors de la création des users tests\n";
$req2->closeCursor();

$blob1 = file_get_contents('../assets/images/test1.jpeg');
$blob2 = file_get_contents('../assets/images/test2.jpeg');
$blob3 = file_get_contents('../assets/images/test3.png');
$blob4 = file_get_contents('../assets/images/test4.png');
$blob5 = file_get_contents('../assets/images/test5.jpeg');
$blob6 = file_get_contents('../assets/images/test6.png');
$blob7 = file_get_contents('../assets/images/test7.jpeg');
$sql3 = "INSERT INTO images (`nom_image`, `img_taille`, `img_type`, `id_uid`, `nb_like`, `nb_comment`, `img_blob`)
    VALUES
('test1', 0, 'jpeg', '1', 0, 0, '".addslashes($blob1)."'),
('test2', 0, 'jpeg', '1', 0, 0, '".addslashes ($blob2)."'),
('test3', 0, 'png', '1', 0, 0, '".addslashes ($blob3)."'),
('test4', 0, 'png', '1', 0, 0, '".addslashes ($blob4)."'),
('test5', 0, 'jpeg', '2', 0, 0, '".addslashes ($blob5)."'),
('test6', 0, 'png', '2', 0, 0, '".addslashes ($blob6)."'),
('test7', 0, 'jpeg', '2', 0, 0, '".addslashes ($blob7)."');
 ";
$req3 = $bdd->prepare($sql3);
$result3 = $req3->execute();
if ($result3)
    echo "Created images tests\n<br>";
else
    echo "Erreur lors de la création des images tests\n";
$req3->closeCursor();
