<?php
//$db->exec('CREATE DATABASE db_camagru');
	require "database.php";
	$bdd2 = new PDO('mysql:'.$DB_HOST, $DB_USER, $DB_PASSWORD);
        $bdd2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $bdd2->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$sql = 'CREATE DATABASE IF NOT EXISTS db_camagru';
	$req = $bdd2->prepare($sql);
	$result = $req->execute();
	if ($result)
	{
		echo "Base de données créée correctement\n";
	} 
	else
	{
		echo "Erreur lors de la création de la base de données \n";
	}
	$req->closeCursor();

function connect(){
        require "database.php";
        try{
                $bdd = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
                $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch(PDOException $e){
                echo "La base de donnée n'est pas disponible, merci de rééssayer plus tard.\n";
        }
        return($bdd);
}

$bdd = connect();
	$sql1 = "CREATE TABLE IF NOT EXISTS users (
	idUsers int (11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
	uidUsers TINYTEXT NOT NULL,
	emailUsers TINYTEXT NOT NULL,
	pwdUsers LONGTEXT NOT NULL,
	activated BIT NOT NULL DEFAULT 0,
    receiveMail int (2) NOT NULL DEFAULT 1,
	keyf VARCHAR (52)
);";
$req2 = $bdd->prepare($sql1);
$result1 = $req2->execute();
if ($result1)
	echo "Created users table\n<br>";
else
	echo "Erreur lors de la création de la table users \n";
$req2->closeCursor();

//Creation table images
$sql2 = "CREATE TABLE IF NOT EXISTS images (
	idimage int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nom_image varchar(50) CHARACTER SET utf8 NOT NULL,
	img_taille VARCHAR(25) NOT NULL,
	img_type VARCHAR(25) NOT NULL,
	id_uid int(11) NOT NULL,
	nb_like int(11) DEFAULT 0,
	nb_comment int(11) DEFAULT 0,
	img_blob LONGBLOB NOT NULL,
	FOREIGN KEY (`id_uid`) REFERENCES `users` (`idUsers`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`id_uid`) REFERENCES `users` (`idUsers`)
);";
$req2 = $bdd->prepare($sql2);
$result2 = $req2->execute();
if ($result2)
	echo "Created images table\n<br>";
else
	echo "Erreur lors de la création de la table images \n";
$req2->closeCursor();

//Creation table comments
$sql3 = "CREATE TABLE IF NOT EXISTS comments (
	idcomment int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	stockcomment varchar(100) CHARACTER SET utf8 NOT NULL,
	id_img int(11) NOT NULL,
	id_usr_com int(11) NOT NULL,
	FOREIGN KEY (`id_img`) REFERENCES `images` (`idimage`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`id_img`) REFERENCES `images` (`idimage`),
	FOREIGN KEY (`id_usr_com`) REFERENCES `users` (`idUsers`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`id_usr_com`) REFERENCES `users` (`idUsers`)
);";
$req3 = $bdd->prepare($sql3);
$result3 = $req3->execute();
if ($result3)
	echo "Created comments table\n<br>";
else
	echo "Erreur lors de la création de la table comments \n";
$req3->closeCursor();

//Creation table managelike
$sql4 = "CREATE TABLE IF NOT EXISTS managelike (
	idLike int (11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
        idimg int(11) NOT NULL,
        id_usr_like int(11) NOT NULL,
        FOREIGN KEY (`idimg`) REFERENCES `images` (`idimage`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (`idimg`) REFERENCES `images` (`idimage`),
        FOREIGN KEY (`id_usr_like`) REFERENCES `users` (`idUsers`) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (`id_usr_like`) REFERENCES `users` (`idUsers`)
);";
$req4 = $bdd->prepare($sql4);
$result4 = $req4->execute();
if ($result4)
        echo "Created managelike table\n<br>";
else
        echo "Erreur lors de la création de la table managelike \n";
$req4->closeCursor();


//Creation table filters
$sql5 = "CREATE TABLE IF NOT EXISTS filters (
    id_filter int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
    filter varchar(255) NOT NULL
  );";
$req5 = $bdd->prepare($sql5);
if ($req5->execute())
    echo "filters table created\n<br>";
$req5->closeCursor();
$sql6 = "INSERT INTO filters (filter)
        VALUES
        ('imgs/spidey.png'),
        ('imgs/strange.png'),
        ('imgs/capameri.png'),
        ('imgs/thor2.png'),
        ('imgs/groot.png'),
        ('imgs/deadpool.png'),
        ('imgs/capameri2.png'),
        ('imgs/capmarv1.png'),
        ('imgs/wolverine.png')";
$req6 = $bdd->prepare($sql6);
if ($req6->execute())
    echo "filters inserted in the table.\n<br>";

?>
